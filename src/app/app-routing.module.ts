import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'splash', pathMatch: 'full' },
  { path: 'splash', loadChildren: './pages/splash/splash.module#SplashPageModule' },
  { path: 'accueil', loadChildren: './pages/accueil/accueil.module#AccueilPageModule' },
  { path: 'edit/:id', loadChildren: './pages/edit/edit.module#EditPageModule' },
  { path: 'add', loadChildren: './pages/add/add.module#AddPageModule' },
 
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
