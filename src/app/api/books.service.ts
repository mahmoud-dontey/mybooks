import { Injectable } from '@angular/core';
//import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};



@Injectable({
  providedIn: 'root'
})



export class BooksService {
  bookUrl:any;
  constructor(private http: HttpClient) {
    this.bookUrl='http://bookstore.dionisly.com/api/book/'
   }

  getBooks(){
    return this.http.get(this.bookUrl)
    //.map(res => res.json());
  }

  removeBook(id) {
    const url = `${this.bookUrl}?id=${id}`;
    return this.http.delete(url,httpOptions)
     // .map(res => res.json());
  }

  addBook(book){
   return this.http.post(this.bookUrl,book, httpOptions ) 
  }

  editBook(book){
    return this.http.put(this.bookUrl,book, httpOptions ) 
   }
}
