import { BooksService } from './../../api/books.service';
import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Vibration } from '@ionic-native/vibration/ngx';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.page.html',
  styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {
  books:any; bgcolor:any; alert:any;
  constructor( private bs:BooksService, public alertCtrl: AlertController,
    private router: Router, private vibration: Vibration) { }

  ngOnInit() {
      
  }
  ionViewDidEnter(){
    this.getMyBooks();
  }
  getMyBooks(){
    this.bs.getBooks()
    .subscribe(data=>{
      this.books=data;
    })
  }
  rmBook={
    id:''
  }
  async removeBook(idBook){
    this.rmBook.id=idBook;
    const alert = await  this.alertCtrl.create({
     // title: 'Déconnexion',
      message: 'Are you sure you want to delete this book ?',
      buttons: [
        {
          text: 'Oui',
          handler: () => {
           this.bs.removeBook(idBook)
           .subscribe(data=>{
            this.vibration.vibrate(1000);
             if(data='deleted successfully'){
               this.books=[];
               this.getMyBooks();
               //this.alertCtrl.dismiss();
             }
           }) 
          }
        },
        {
          text: 'Non',
          role: 'cancel',
          
          handler: () => {

          }
        }
      ]
    });
    
    await alert.present();
    

  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.books=[];
    this.getMyBooks();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  editBook(id){
    this.router.navigate(['/edit', id]);
  }

  addBook(){
    this.router.navigateByUrl('/add')
  }

  getPrice(price){
   alert('This book costs: '+ price+ '$'); 
  }


}
