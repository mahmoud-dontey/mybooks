import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { BooksService } from './../../api/books.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Vibration } from '@ionic-native/vibration/ngx';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  book:any;
  constructor( private bs: BooksService, private router: Router,
    private alertCtrl: AlertController, private vibration: Vibration) { 
    this.book = new FormGroup({
      name: new FormControl('',[Validators.required]),
      price: new FormControl('', [Validators.required]),
      availability: new FormControl('', [Validators.required]),
      isbn: new FormControl('', [Validators.required])
    });

    
  }
  

  ngOnInit() {
  }

   onSubmit() {
    this.bs.addBook(this.book.value)
    .subscribe(data=>{
      this.vibration.vibrate(1000);
      this.router.navigateByUrl('/accueil');
    },
    error => {
      alert('something went wrong, please verify your data');
    })
  }

  

}
