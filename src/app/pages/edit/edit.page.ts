import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BooksService } from './../../api/books.service';




@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  id: any; name:any; price:any;isbn:any; availability:any
  book: any; books:any;
  constructor(private route: ActivatedRoute, private bs: BooksService, 
    private router: Router) {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    this.book = new FormGroup({
      id: new FormControl(this.id),
      name: new FormControl('',[Validators.required]),
      price: new FormControl('', [Validators.required]),
      availability: new FormControl('', [Validators.required]),
      isbn: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
  }
  ionViewDidEnter(){
   this.bs.getBooks()
   .subscribe(data=>{
     this.books=data;
     for (let i = 0; i < this.books.length; i++) {
       if(this.books[i].id==this.id){
         console.log('trouvé !',this.books[i])
        this.name=this.books[i].name;
        this.price=this.books[i].price;
        this.isbn=this.books[i].isbn;
        this.availability= this.books[i].availability
       }
     }
     
   })
  }

  onSubmit() {
    this.bs.editBook(this.book.value)
    .subscribe(data=>{
      this.router.navigateByUrl('/accueil');
    },
    error => {
      alert('something went wrong, please verify your data');
    })
   
  }

}
